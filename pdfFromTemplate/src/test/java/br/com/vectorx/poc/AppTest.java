package br.com.vectorx.poc;

/**
 * @author brethel
 */
import static org.fest.assertions.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

	private final static SimpleDateFormat _sdf = new SimpleDateFormat("YYYY_MM_dd");

	// @see
	// http://stackoverflow.com/questions/19635275/how-to-generate-multiple-lines-in-pdf-using-apache-pdfbox
	private final static List<String> mensagem = new LinkedList<>(
			Arrays.asList(	"As armas e os barões assinalados,",
							"que da Ocidental praia Lusitana,", 
							"por mares nunca dantes navegados,", 
							"passaram ainda além da Taprobana.", 
							"",
							"E entre perigos e guerras esforçados,", 
							"mais do que prometia a força humana,", 
							"e entre gente remota edificaram,",
							"novo Reino que tanto sublimaram.")
			);

	private final static String[] msg = new String[] { "lala", "lele", "lili", "lolo lulu-zinha, a famosa personagem de quadrinhos que conquistou milhões de fãs" };

	@Test
	public void editPDF() {
		PDDocument document = null;

		StringBuilder sb = new StringBuilder().append("/tmp/mypdf_").append(_sdf.format(new Date())).append(".pdf");

		try {

			document = PDDocument.load(new File(this.getClass().getResource("/template.pdf").getFile()));
			PDPage page = (PDPage) document.getDocumentCatalog().getAllPages().get(0);
			PDFont font = PDType1Font.HELVETICA_BOLD;
			PDPageContentStream contentStream = new PDPageContentStream(document, page, true, true);
			page.getContents().getStream();
			contentStream.beginText();
			contentStream.setFont(font, 12);

			PDRectangle mediabox = page.findMediaBox();
			float margin = 60;
			float width = mediabox.getWidth() - 2 * margin;
			float startX = mediabox.getLowerLeftX() + margin;
			float startY = mediabox.getUpperRightY() - margin;

			contentStream.moveTextPositionByAmount(startX, startY - 50);
			for (String linha : mensagem) {
				contentStream.moveTextPositionByAmount(0, -15); // subtrai para
																// descer a
																// linha...
				contentStream.drawString(linha);
			}

			contentStream.endText();
			contentStream.close();

			document.save(sb.toString());
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (COSVisitorException e) {
			e.printStackTrace();
		}
		System.err.println("wrote [" + sb.toString() + "].");
		assertThat(true).isTrue();
	}

	@Test
	public void fillForm() throws IOException, COSVisitorException {
		// @see
		// http://www.maketecheasier.com/create-a-pdf-with-fillable-forms-in-libreoffice/

		// @see http://www.daedtech.com/programatically-filling-out-pdfs-in-java
		// @see
		// http://stackoverflow.com/questions/14454387/pdfbox-how-to-flatten-a-pdf-form
		PDDocument document = PDDocument.load(new File(this.getClass().getResource("/template2.pdf").getFile()));

		printFields(document);

		StringBuilder sb = new StringBuilder().append("/tmp/myForm_").append(_sdf.format(new Date())).append(".pdf");

		for (int i = 1; i < 5; i++) {
			setField(document, "Texto0" + i, msg[i - 1]);
		}

		document.save(sb.toString());
		document.close();

		System.err.println("wrote [" + sb.toString() + "].");
		assertThat(true).isTrue();
	}

	@Test
	public void fillTable() throws IOException, COSVisitorException {
		// @see
		// http://fahdshariff.blogspot.com.br/2010/10/creating-tables-with-pdfbox.html

		StringBuilder sb = new StringBuilder().append("/tmp/myTable_").append(_sdf.format(new Date())).append(".pdf");

		PDDocument doc = PDDocument.load(new File(this.getClass().getResource("/template.pdf").getFile()));
		PDPage page = (PDPage) doc.getDocumentCatalog().getAllPages().get(0);
		
		PDPageContentStream contentStream = new PDPageContentStream(doc, page, true, true);

		String[][] content = { 	{ "a", "b", "1", "alpha"   }, 
								{ "c", "d", "2", "beta"    }, 
								{ "e", "f", "3", "gamma"   }, 
								{ "g", "h", "4", "delta"   }, 
								{ "i", "j", "5", "epsilon" },
								{ "k", "l", "6", "phi"     } };

		drawTable(page, contentStream, 700, 100, content);
		
		// more pages... copies.
		doc.addPage(page);
		
		contentStream.close();
		doc.save(sb.toString());
		System.err.println("wrote [" + sb.toString() + "].");

		assertThat(true).isTrue();
	}

	//-------------------------------------------------------------------------
	private void drawTable(	PDPage page, PDPageContentStream contentStream, 
							float y, float margin, String[][] content)	
				throws IOException 
	{
		final int rows = content.length;
		final int cols = content[0].length;
		final float rowHeight = 20f;
		final float tableWidth = page.findMediaBox().getWidth() - (2 * margin);
		final float tableHeight = rowHeight * rows;
		final float colWidth = tableWidth / (float) cols;
		final float cellMargin = 5f;

		// draw the rows
		float nexty = y;
		for (int i = 0; i <= rows; i++) {
			contentStream.drawLine(margin, nexty, margin + tableWidth, nexty);
			nexty -= rowHeight;
		}

		// draw the columns
		float nextx = margin;
		for (int i = 0; i <= cols; i++) {
			contentStream.drawLine(nextx, y, nextx, y - tableHeight);
			nextx += colWidth;
		}

		// now add the text
		contentStream.setFont(PDType1Font.HELVETICA, 10);

		float textx = margin + cellMargin;
		float texty = y - 15;
		for (int i = 0; i < content.length; i++) {
			for (int j = 0; j < content[i].length; j++) {
				String text = content[i][j];
				contentStream.beginText();
				contentStream.moveTextPositionByAmount(textx, texty);
				contentStream.drawString(text);
				contentStream.endText();
				textx += colWidth;
			}
			texty -= rowHeight;
			textx = margin + cellMargin;
		}
	}

	private void setField(PDDocument _pdfDocument, String fieldName, String value) 
			throws IOException 
	{
		PDDocumentCatalog docCatalog = _pdfDocument.getDocumentCatalog();
		PDAcroForm acroForm = docCatalog.getAcroForm();
		PDField field = acroForm.getField(fieldName);
		if (field != null) {
			field.setValue(value);

			field.setReadonly(true);
		} else {
			System.err.println("No field found with name:" + fieldName);
		}
	}

	@SuppressWarnings("rawtypes")
	private void printFields(PDDocument _pdfDocument) throws IOException {
		PDDocumentCatalog docCatalog = _pdfDocument.getDocumentCatalog();
		PDAcroForm acroForm = docCatalog.getAcroForm();
		List fields = acroForm.getFields();
		Iterator fieldsIter = fields.iterator();

		System.out.println(new Integer(fields.size()).toString() + " top-level fields were found on the form");

		while (fieldsIter.hasNext()) {
			PDField field = (PDField) fieldsIter.next();
			processField(field, "|--", field.getPartialName());
		}
	}

	@SuppressWarnings("rawtypes")
	private static void processField(PDField field, String sLevel, String sParent) throws IOException {
		List kids = field.getKids();
		if (kids != null) {
			Iterator kidsIter = kids.iterator();
			if (!sParent.equals(field.getPartialName())) {
				sParent = sParent + "." + field.getPartialName();
			}

			System.out.println(sLevel + sParent);

			while (kidsIter.hasNext()) {
				Object pdfObj = kidsIter.next();
				if (pdfObj instanceof PDField) {
					PDField kid = (PDField) pdfObj;
					processField(kid, "|  " + sLevel, sParent);
				}
			}
		} else {
			String outputString = sLevel + sParent + "." + field.getPartialName() + ",  type=" + field.getClass().getName();
			System.out.println(outputString);
		}
	}

}
