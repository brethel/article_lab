package br.com.vectorx.poc;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)

@Suite.SuiteClasses({
	br.com.vectorx.poc.jcr.JCRTests.class,
	br.com.vectorx.poc.jpa.JPATests.class
})
public class AllTests {
}
