package br.com.vectorx.poc.jcr;

import javax.jcr.LoginException;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.infinispan.schematic.document.ParsingException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modeshape.jcr.ConfigurationException;
import org.modeshape.jcr.ModeShapeEngine;
import org.modeshape.jcr.RepositoryConfiguration;
import org.modeshape.jcr.api.JcrTools;

public class JCRTests {
	
	static Repository repo;
	static ModeShapeEngine engine;
	static String repositoryName;
	
	/**
	 * @see http://www.mastertheboss.com/jboss-frameworks/modeshape/modeshape-quickstart-tutorial
	 */
	@BeforeClass
	public static void setup() {
		try {
			engine = new ModeShapeEngine();
			engine.start();
			
			RepositoryConfiguration config = RepositoryConfiguration.read( 
					JCRTests.class.getClassLoader().getResource("repository.config") 
			);
			repo = engine.deploy(config);
			repositoryName = config.getName();
			
		} catch (ParsingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	@Test
	public void isRepositoryAvailable() {
		try {
			JcrTools tools = new JcrTools();
			Session sess = repo.login();
			
			Assert.assertTrue( sess.isLive() );
			
			sess.logout();
			
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
