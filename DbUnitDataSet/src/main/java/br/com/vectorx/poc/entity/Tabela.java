package br.com.vectorx.poc.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by brethel on 10/11/14.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@SequenceGenerator(name="seq0", sequenceName="my_seq", allocationSize=1)
public class Tabela implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq0")
    @Getter
    @Setter
    private Long id;

    @Column
    @Getter
    @Setter
    private String name;

    @Column
    @Getter
    @Setter
    private Date dtCreation;

    @Transient
    @Getter
    @Setter
    private String comment;

    public Tabela() {}

    public Tabela(long l) {
        this.id = new Long(l);
    }
}
