package br.com.vectorx.poc;

import br.com.vectorx.poc.entity.Tabela;
import org.junit.BeforeClass;
import org.junit.Test;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Create the test case
     */

	private static EntityManager em = null;

	@BeforeClass
	public static void setup() {
		if (em == null) {
			em = Persistence.createEntityManagerFactory("testPUifx").createEntityManager();
		}
    }

	@Test
	public void connectTo() {
		em.getTransaction().begin();
		Tabela tbl = new Tabela();
		em.persist(tbl);
		em.getTransaction().commit();
 	}

	@Test
	public void insertData() {
		assertTrue(true);
	}

}
