package br.com.vectorx.poc;

/**
 * <p>
 * Testing on how to populate a table with a data set defined on an agnostic way 
 * ( DbUnit + Hibernate + JPA2 )
 * </p>
 * 
 * ( boundary | control | entity ) -> microservices on jboss_domain
 * 
 */
public class App {
    public static void main( String[] args ) {
        System.out.println( "Hello World!" );
    }
}
