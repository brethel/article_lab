package br.com.vectorx.poc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by brethel on 10/11/14.
 */
@Entity
@Table(name="tabela")
@XmlAccessorType(value=XmlAccessType.FIELD)

@SequenceGenerator(	name="seq0", 
					sequenceName="my_seq", 
					allocationSize=1
)
public class Tabela implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq0")
    @Getter
    @Setter
    private Long id;

    @Column
    @Getter
    @Setter
    private String name;

    @Column
    @Getter
    @Setter
    private Date dtCreation;

    @Getter
    @Setter
    private String comment;

    public Tabela() {}

    public Tabela(long l) {
        this.id = new Long(l);
    }

	@Override
	public String toString() {
		return "Tabela [id=" + id + ", name=" + name + ", dtCreation=" + dtCreation + ", comment=" + comment + "]";
	}
    
}
