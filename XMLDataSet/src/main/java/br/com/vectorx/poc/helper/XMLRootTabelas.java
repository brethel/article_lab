package br.com.vectorx.poc.helper;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import br.com.vectorx.poc.entity.Tabela;

@XmlAccessorType(value=XmlAccessType.FIELD)
@XmlRootElement(name="tabelas")
public class XMLRootTabelas {
	
	@Setter
	@Getter
	@XmlElement(name="tabela")
	public List<Tabela> tabelas;

	public XMLRootTabelas(){}
}