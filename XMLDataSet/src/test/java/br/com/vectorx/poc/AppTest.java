package br.com.vectorx.poc;

import static org.junit.Assert.assertTrue;

import java.io.File;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.Persistence;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.BeforeClass;
import org.junit.Test;

import br.com.vectorx.poc.entity.Tabela;
import br.com.vectorx.poc.helper.XMLRootTabelas;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Create the test case
     */

	private static EntityManager em = null;

	@BeforeClass
	public static void setup() throws Exception {
		if (em == null) {
			em = Persistence.createEntityManagerFactory("testPUifx").createEntityManager();
			em.setFlushMode(FlushModeType.COMMIT);
		}
		
    }

	@Test
	public void connectTo() {
		assertTrue(em.isOpen());
 	}

	@Test
	public void insertData() throws JAXBException {
		File f = new File(this.getClass().getResource("/dataset.xml").getFile());
		JAXBContext jaxbContext = JAXBContext.newInstance(XMLRootTabelas.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		XMLRootTabelas lista = (XMLRootTabelas) jaxbUnmarshaller.unmarshal(f);
		
		
		em.getTransaction().begin();
		for ( Tabela t : lista.getTabelas() ) {
			//System.out.println( t.toString() );
			em.persist(t);
		}
		em.flush();
		em.getTransaction().commit();
		em.close();
		
		assertTrue(!em.isOpen());
	}

}
